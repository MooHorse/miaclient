﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace MiaClient
{
    class MethodsClass
    {

        //Method to restart services passed from Mia
        public static string RestartServices(ServiceController ServiceName)
        {
            try
            {
                ServiceName.Stop();
                ServiceName.WaitForStatus(ServiceControllerStatus.Stopped);
                ServiceName.Start();
                ServiceName.WaitForStatus(ServiceControllerStatus.Running);
                return "Services Restarted";
            }
            catch
            {
                return "Unable to restart services";
            }
        }

        //Method to stop services passed from Mia
        public static string StopService(ServiceController ServiceName)
        {
            try
            {
                if (ServiceName.Status == ServiceControllerStatus.Running)
                {
                    ServiceName.Stop();
                    ServiceName.WaitForStatus(ServiceControllerStatus.Stopped);
                    return "Service Stopped";
                }
                else
                {
                    return "Service not running";
                }
            }
            catch
            {
                return "Unable to stop service";
            }
        }

        //Method to Start Services Passed from Mia
        public static string StartService(ServiceController ServiceName)
        {
            try
            {
                if (ServiceName.Status == ServiceControllerStatus.Stopped)
                {
                    ServiceName.Start();
                    ServiceName.WaitForStatus(ServiceControllerStatus.Running);
                    return "Service Started";
                }
                else
                {
                    return "Service not running";
                }
            }
            catch
            {
                return "Unable to stop service";
            }
        }

        //Open Socket Connection to MIA
        public static string Connect(string serverIP, string message, int port)
        {
            string output = "";
            
            try
            {
                // Create a TcpClient. 
                // The client requires a TcpServer that is connected to the same address specified by the server and port combination.
                TcpClient client = new TcpClient(serverIP, port);
                //client.SendTimeout = 1000;
                //client.ReceiveTimeout = 1000;

                // Translate the passed message into ASCII and store it as a byte array.
                byte[] data;// = new byte[256];
                data = Encoding.ASCII.GetBytes(message);

                // Get a client stream for reading and writing. 
                // Stream stream = client.GetStream();
                Stream stream = client.GetStream();
                var bin = new BinaryFormatter();
                var list = (List<string>)bin.Deserialize(stream);

                // Send the message to the connected TcpServer. 
                stream.Write(data, 0, data.Length);

                // Buffer to store the response bytes.
                data = new byte[stream.Length];

                // String to store the response ASCII representation.
                string responseData = string.Empty;

                // Read the first batch of the TcpServer response bytes.
                int bytes = stream.Read(data, 0, data.Length);
                responseData = Encoding.ASCII.GetString(data, 0, bytes);
                output = responseData;

                //Clean up connections
                client.Close();
                stream.Close();
                return output;
                
            }
            catch (ArgumentNullException)
            {
                output = "ArgumentNullException";
                return output;
            }
            catch (SocketException)
            {
                output = "SocketException";
                return output;
            }
            catch (Exception e)
            {
                output = e.ToString();
                return output;
            }
        }

        //Ping Functionality for Client
        public static string PingService(string ipToPing)
        {
            while (true)
            {
                try
                {
                    Ping p = new Ping();
                    PingReply r = p.Send(ipToPing);
                    p.Dispose();

                    if (r.Status == IPStatus.Success)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Failure";
                    }
                }
                catch (Exception)
                {
                    return "Failure";
                }
            }
        }

        //output all logs to Codama
        public static void WindowsApplicationSyslog(string ServerName, IPAddress SocketServerIPorDNS, string SyslogServerPort, string logType)
        {
            string LogInQuestion = "";

            if (logType == "Application")
            {
                var MyIni = new IniFile("ApplicationLog.ini");
                LogInQuestion = MyIni.Read("LastKnownApplicationEventID", "MyProg");
            }
            else if (logType == "Security")
            {
                var MyIni = new IniFile("SecurityLog.ini");
                LogInQuestion = MyIni.Read("LastKnownSecurityEventID", "MyProg");
            }
            else if (logType == "System")
            {
                var MyIni = new IniFile("SystemLog.ini");
                LogInQuestion = MyIni.Read("LastKnownSystemEventID", "MyProg");
            }

            string SocketServerMessage;

            while (true)
            {
                int LogBeingUsed = int.Parse(LogInQuestion);
                try
                {
                    EventLog ev = new EventLog(logType, Environment.MachineName);
                    int LastLogToShow = ev.Entries.Count;
                    string output = "";

                    EventLogEntry entry;
                    EventLogEntryCollection entries = ev.Entries;
                    Stack<EventLogEntry> stack = new Stack<EventLogEntry>();
                    for (int i = 0; i < entries.Count; i++)
                    {
                        entry = entries[i];
                        stack.Push(entry);

                        if (entry.TimeGenerated >= DateTime.Now.AddMinutes(-30))
                        {
                            if (entry.Index > LogBeingUsed)
                            {
                                entry = stack.Pop();// only display the last record
                                SocketServerMessage = "\n[EventID]\t" + entry.InstanceId +
                                    "\n[TimeWritten]\t" + entry.TimeWritten +
                                    "\n[TimeGenerated]\t" + entry.TimeGenerated +
                                    "\n[LogType]\t" + logType +
                                    "\n[ClientName]\t" + ServerName +
                                    "\n[Source]\t" + entry.Source +
                                    "\n[Message]\t" + entry.Message;

                                SyslogSocketClient(SocketServerIPorDNS, SyslogServerPort, SocketServerMessage);
                                LogInQuestion = entry.Index.ToString();

                                if (logType == "Application")
                                {
                                    File.WriteAllText("ApplicationLog.ini", "[MyProg]" + Environment.NewLine + "LastKnownApplicationEventID=" + LogInQuestion);
                                }
                                else if (logType == "Security")
                                {
                                    File.WriteAllText("SecurityLog.ini", "[MyProg]" + Environment.NewLine + "LastKnownSecurityEventID=" + LogInQuestion);
                                }
                                else if (logType == "System")
                                {
                                    File.WriteAllText("SystemLog.ini", "[MyProg]" + Environment.NewLine + "LastKnownSystemEventID=" + LogInQuestion);
                                }
                            }
                        }
                    }
                    ev.Close();
                    stack.Clear();
                }
                catch (Exception wtf)
                {
                    SyslogSocketClient(SocketServerIPorDNS, SyslogServerPort, wtf.Message);

                }
                Thread.Sleep(60000);
            }
        }

        //Syslog Client to send Data to Codama (Kevin Syslog)
        public static void SyslogSocketClient(IPAddress SocketServerIP, string SocketServerPort, string SocketServerMessage)
        {
            try
            {
                byte[] data = new byte[1024];
                
                IPAddress address = Dns.GetHostAddresses(SocketServerIP.ToString())[0];

                IPEndPoint ip = new IPEndPoint(address, int.Parse(SocketServerPort));

                Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                data = Encoding.ASCII.GetBytes(SocketServerMessage);
                server.SendTo(data, data.Length, SocketFlags.None, ip);

                server.Close();
            }
            catch
            {

            }
        }
        
        //How to action based on what the SocketServer Responded
        private void ActOnServerCommands(string serverResponded)
        {
            var MyIni = new IniFile("Config.ini");
            
            ServiceController MediaServer = new ServiceController(MyIni.Read("MediaServerName", "MyProg"));
            ServiceController PhoneSystemDatabaseServer = new ServiceController(MyIni.Read("PhoneSystemDatabaseServerName", "MyProg"));
            ServiceController RTPTunnelService = new ServiceController(MyIni.Read("RTPTunnelServiceName", "MyProg"));
            ServiceController SipServerService = new ServiceController(MyIni.Read("SipServerServiceName", "MyProg"));
            ServiceController QueueManagerService = new ServiceController(MyIni.Read("QueueManagerServiceName", "MyProg"));
            ServiceController ParkingOrbitService = new ServiceController(MyIni.Read("ParkingOrbitServiceName", "MyProg"));
            ServiceController FaxService = new ServiceController(MyIni.Read("FaxServiceName", "MyProg"));
            ServiceController DigitalReceptionistService = new ServiceController(MyIni.Read("DigitalReceptionistServiceName", "MyProg"));
            ServiceController ConfigService = new ServiceController(MyIni.Read("ConfigServiceName", "MyProg"));
            ServiceController ConferenceService = new ServiceController(MyIni.Read("ConferenceServiceName", "MyProg"));
            ServiceController HistoryService = new ServiceController(MyIni.Read("HistoryServiceName", "MyProg"));
            ServiceController NotificationService = new ServiceController(MyIni.Read("NotificationServiceName", "MyProg"));
            
            //If MIA tells a client to restart the 3CX services.
            if (serverResponded == "Restart all 3CX Services")
            {

                RestartServices(MediaServer);
                RestartServices(PhoneSystemDatabaseServer);
                RestartServices(RTPTunnelService);
                RestartServices(SipServerService);
                RestartServices(QueueManagerService);
                RestartServices(ParkingOrbitService);
                RestartServices(FaxService);
                RestartServices(DigitalReceptionistService);
                RestartServices(ConfigService);
                RestartServices(ConferenceService);
                RestartServices(HistoryService);
                RestartServices(NotificationService);
            }
            else if (serverResponded == "Stop all 3CX Services")
            {
                StopService(MediaServer);
                StopService(PhoneSystemDatabaseServer);
                StopService(RTPTunnelService);
                StopService(SipServerService);
                StopService(QueueManagerService);
                StopService(ParkingOrbitService);
                StopService(FaxService);
                StopService(DigitalReceptionistService);
                StopService(ConfigService);
                StopService(ConferenceService);
                StopService(HistoryService);
                StopService(NotificationService);
            }
            else if (serverResponded == "Start 3CX Media service")
            {
                StartService(MediaServer);
            }
            else if (serverResponded == "Start 3CX Database service")
            {
                StartService(PhoneSystemDatabaseServer);
            }
            else if (serverResponded == "Start 3CX Tunnel service")
            {
                StartService(PhoneSystemDatabaseServer);
            }
            else if (serverResponded == "3CX SIP service is no longer running")
            {
                StartService(SipServerService);
            }
            else if (serverResponded == "Start 3CX QueueManager service")
            {
                StartService(QueueManagerService);
            }
            else if (serverResponded == "Start 3CX ParkingOrbit service")
            {
                StartService(ParkingOrbitService);
            }
            else if (serverResponded == "Start 3CX Fax service")
            {
                StartService(FaxService);
            }
            else if (serverResponded == "Start 3CX IVR service")
            {
                StartService(DigitalReceptionistService);
            }
            else if (serverResponded == "Start 3CX Config service")
            {
                StartService(ConfigService);
            }
            else if (serverResponded == "Start 3CX Conferencing service")
            {
                StartService(ConferenceService);
            }
            else if (serverResponded == "Start 3CX History service")
            {
                StartService(HistoryService);
            }
            else if (serverResponded == "Start 3CX Notification service")
            {
                StartService(NotificationService);
            }
            else
            {

            }
        }

    }
}
