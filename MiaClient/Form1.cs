﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Threading;
using System.Net;
using System.IO;

namespace MiaClient
{
    public partial class Form1 : Form
    {
        //Variables to hunt for in .ini File (In order)
        string MIAServerIP;
        string port;
        int portNumber;
        string ServerName;
        string ClientID;
        string MediaServerName;
        string PhoneSystemDatabaseServerName;
        string RTPTunnelServiceName;
        string SipServerServiceName;
        string QueueManagerServiceName;
        string ParkingOrbitServiceName;
        string FaxServiceName;
        string DigitalReceptionistServiceName;
        string ConfigServiceName;
        string ConferenceServiceName;
        string HistoryServiceName;
        string NotificationServiceName;
        string SyslogServerIP;
        string SyslogServerPort;

        #region Declare all global variables
        
        //Declare Threads
        Thread CommunicateWithMia;
        Thread MonitorServicesAsNeeded;
        Thread CheckSipServerReachable;
        Thread SyslogOutput_Application;
        Thread SyslogOutput_Security;
        Thread SyslogOutput_System;

        //Declare Changing Variables
        string socketResponse;
        
        #endregion

        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            #region Declare all variables as they are read from the INI file
            var MyIni = new IniFile("Config.ini");
            MIAServerIP = MyIni.Read("MIAServerIP", "MyProg");
            port = MyIni.Read("SocketConnectionPort", "MyProg");
            ServerName = MyIni.Read("ServerName", "MyProg");
            ClientID = MyIni.Read("ClientID", "MyProg");
            MediaServerName = MyIni.Read("MediaServerName", "MyProg");
            PhoneSystemDatabaseServerName = MyIni.Read("PhoneSystemDatabaseServerName", "MyProg");
            RTPTunnelServiceName = MyIni.Read("RTPTunnelServiceName", "MyProg");
            SipServerServiceName = MyIni.Read("SipServerServiceName", "MyProg");
            QueueManagerServiceName = MyIni.Read("QueueManagerServiceName", "MyProg");
            ParkingOrbitServiceName = MyIni.Read("ParkingOrbitServiceName", "MyProg");
            FaxServiceName = MyIni.Read("FaxServiceName", "MyProg");
            DigitalReceptionistServiceName = MyIni.Read("DigitalReceptionistServiceName", "MyProg");
            ConfigServiceName = MyIni.Read("ConfigServiceName", "MyProg");
            ConferenceServiceName = MyIni.Read("ConferenceServiceName", "MyProg");
            HistoryServiceName = MyIni.Read("HistoryServiceName", "MyProg");
            NotificationServiceName = MyIni.Read("NotificationServiceName", "MyProg");
            SyslogServerIP = MyIni.Read("SyslogServerIP", "MyProg");
            SyslogServerPort = MyIni.Read("SyslogServerPort", "MyProg");
            #endregion

            #region Create array for 3CX Service Variables

            string[] servicesToMonitor = new string[]
                {
                    MediaServerName,
                    PhoneSystemDatabaseServerName,
                    RTPTunnelServiceName,
                    SipServerServiceName,
                    QueueManagerServiceName,
                    ParkingOrbitServiceName,
                    FaxServiceName,
                    DigitalReceptionistServiceName,
                    ConfigServiceName,
                    ConferenceServiceName,
                    HistoryServiceName,
                    NotificationServiceName
                };
            
            #endregion

            #region Run Basic Startup Checks

            //if (ServerName == "ClientNameHere")
            //{
            //    MessageBox.Show("Please give this server a valid name!");
            //    Application.Exit();
            //}

            #endregion

            #region Set default Values where needed

            //Set Logging values to 0 in case of Server Reboot.
            File.WriteAllText("ApplicationLog.ini", "[MyProg]" + Environment.NewLine + "LastKnownApplicationEventID=0");
            File.WriteAllText("SecurityLog.ini", "[MyProg]" + Environment.NewLine + "LastKnownSecurityEventID=0");
            File.WriteAllText("SystemLog.ini", "[MyProg]" + Environment.NewLine + "LastKnownSystemEventID=0");

            #endregion

            #region Startup Threads
            
            //Thread to check if Porta is reachable. (This will only mark porta as unreachable if 30 pings fail to Porta.)
            CheckSipServerReachable = new Thread(new ThreadStart(CheckSipServer));
            CheckSipServerReachable.IsBackground = true;
            CheckSipServerReachable.Start();

            //Thread to start socket Client
            foreach (string Service in servicesToMonitor)
            {
                MonitorServicesAsNeeded = new Thread(() => Monitor3CXServicesMethod(Service));
                MonitorServicesAsNeeded.IsBackground = true;
                MonitorServicesAsNeeded.Start();
            }

            //Thread to send application log Data to Codama (Kevin Syslog)
            SyslogOutput_Application = new Thread(new ThreadStart(StartReadingSystemLogs_Application));
            SyslogOutput_Application.IsBackground = true;
            SyslogOutput_Application.Start();

            ////Thread to send Security log Data to Codama (Kevin Syslog)
            //SyslogOutput_Security = new Thread(new ThreadStart(StartReadingSystemLogs_Security));
            //SyslogOutput_Security.IsBackground = true;
            //SyslogOutput_Security.Start();

            //Thread to send System log data to Codama (Kevin Syslog)
            SyslogOutput_System = new Thread(new ThreadStart(StartReadingSystemLogs_System));
            SyslogOutput_System.IsBackground = true;
            SyslogOutput_System.Start();

            #endregion

        }

        //Thread for Monitoring SIP Server Connectivity
        private void CheckSipServer()
        {
            lblSipServer.Invoke((MethodInvoker)(() => lblSipServer.Text = "Checking if Porta is reachable"));

            try
            {
                while (true)
                {
                    for (int PortaPingCount = 0; PortaPingCount < 31; PortaPingCount++)
                    {

                        if (MethodsClass.PingService("196.15.213.2") == "Success")
                        {
                            PortaPingCount = 0;
                            lblSipServer.Invoke((MethodInvoker)(() => lblSipServer.Text = "Porta is Reachable"));
                            lblSipServer.Invoke((MethodInvoker)(() => lblSipServer.ForeColor = Color.Green));
                            Thread.Sleep(1000);
                        }
                        else
                        {
                            PortaPingCount++;
                        }
                    }
                    lblSipServer.Invoke((MethodInvoker)(() => lblSipServer.Text = "Checking for Porta failed"));
                    lblSipServer.Invoke((MethodInvoker)(() => lblSipServer.ForeColor = Color.Red));
                    Thread.Sleep(30000);
                }
            }
            catch (Exception ex)
            {
                IPAddress address = Dns.GetHostAddresses(SyslogServerIP)[0];
                MethodsClass.SyslogSocketClient(address, SyslogServerPort, "MIA Client Experienced the following error:" + Environment.NewLine + ex.Message);
                Thread.Sleep(30000);
                CheckSipServer();
            }
        }

        //Thread to monitor any Service name provided.
        private void Monitor3CXServicesMethod(string ServiceToMonitor)
        {
            string MessageToMiaSocketServer = "-" + ClientID + "EOF";
            int.TryParse(port, out portNumber);

            ServiceController ServiceMonitoredByThread = new ServiceController(ServiceToMonitor);
            ServiceController[] services = ServiceController.GetServices();
            
            try
            {
                while (true)
                {
                    // try to find service name
                    foreach (ServiceController service in services)
                    {
                        if (service.ServiceName == ServiceToMonitor)
                        {
                            if (!ServiceMonitoredByThread.Status.Equals(ServiceControllerStatus.Running))
                            {
                                MessageToMiaSocketServer = "3CX Media service is no longer running-" + ClientID + "EOF";
                                socketResponse = MethodsClass.Connect(MIAServerIP, MessageToMiaSocketServer, portNumber);
                            }
                        }
                    }
                    Thread.Sleep(10000);
                }
            }
            catch (Exception ex)
            {
                IPAddress address = Dns.GetHostAddresses(SyslogServerIP)[0];
                MethodsClass.SyslogSocketClient(address, SyslogServerPort, "MIA Client Experienced the following error:" + Environment.NewLine + ex.Message);
                Thread.Sleep(30000);
                Monitor3CXServicesMethod(ServiceToMonitor);
            }
        }
        
        #region Start threads to read Windows Event Logs

        private void StartReadingSystemLogs_Application()
        {
            IPAddress address = Dns.GetHostAddresses(SyslogServerIP)[0];
            MethodsClass.WindowsApplicationSyslog(ServerName, address, SyslogServerPort, "Application");
        }

        private void StartReadingSystemLogs_Security()
        {
            IPAddress address = Dns.GetHostAddresses(SyslogServerIP)[0];
            MethodsClass.WindowsApplicationSyslog(ServerName, address, SyslogServerPort, "Security");
        }

        private void StartReadingSystemLogs_System()
        {
            IPAddress address = Dns.GetHostAddresses(SyslogServerIP)[0];
            MethodsClass.WindowsApplicationSyslog(ServerName, address, SyslogServerPort, "System");
        }

        #endregion

        private void btnTest_Click(object sender, EventArgs e)
        {
            
        }
    }
}
