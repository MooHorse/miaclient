﻿namespace MiaClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSipServer = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblSipServer
            // 
            this.lblSipServer.AutoSize = true;
            this.lblSipServer.ForeColor = System.Drawing.Color.Red;
            this.lblSipServer.Location = new System.Drawing.Point(13, 26);
            this.lblSipServer.Name = "lblSipServer";
            this.lblSipServer.Size = new System.Drawing.Size(56, 13);
            this.lblSipServer.TabIndex = 1;
            this.lblSipServer.Text = "Sip Server";
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(441, 226);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 2;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Visible = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 261);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.lblSipServer);
            this.Name = "Form1";
            this.Text = "MIA Client";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblSipServer;
        private System.Windows.Forms.Button btnTest;
    }
}

