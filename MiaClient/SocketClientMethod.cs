﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using TCX.PBXAPI;

namespace MiaClient
{
    class SocketClientMethod
    {
        public static void Connect(string serverIP, string message, int port)
        {
            string output = "";

            try
            {
                // Create a TcpClient. 
                // The client requires a TcpServer that is connected 
                // to the same address specified by the server and port 
                // combination.
                TcpClient client = new TcpClient(serverIP, port);

                // Translate the passed message into ASCII and store it as a byte array.
                byte[] data = new byte[256];
                data = Encoding.ASCII.GetBytes(message);

                // Get a client stream for reading and writing. 
                // Stream stream = client.GetStream();
                NetworkStream stream = client.GetStream();

                // Send the message to the connected TcpServer. 
                stream.Write(data, 0, data.Length);

                output = "Sent: " + message;
                Console.WriteLine(output);

                // Buffer to store the response bytes.
                data = new byte[256];

                // String to store the response ASCII representation.
                string responseData = string.Empty;

                // Read the first batch of the TcpServer response bytes.
                int bytes = stream.Read(data, 0, data.Length);
                responseData = Encoding.ASCII.GetString(data, 0, bytes);
                output = "Received: " + responseData;

                //A little Housekeeping
                stream.Close();
                client.Close();
                return output;
                
            }
            catch (ArgumentNullException e)
            {
                return e;
            }
            catch (SocketException e)
            {
                return e;
            }
            catch (Exception)
            {
                return "Something Went Wrong";
            }
        }
    }
}
