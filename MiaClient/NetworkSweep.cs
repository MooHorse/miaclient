﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MiaClient
{
    class NetworkSweep
    {
        [DllImport("Iphlpapi.dll", EntryPoint = "SendARP")]
        internal extern static int SendArp(int destIpAddress, int srcIpAddress, byte[] macAddress, ref int macAddressLength);

        public static void StartScan()
        {
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (ni.OperationalStatus == OperationalStatus.Down)
                    continue;
                if (ni.GetIPProperties().GatewayAddresses.Count == 0)
                    continue;
                foreach (UnicastIPAddressInformation uipi in ni.GetIPProperties().UnicastAddresses)
                {
                    if (uipi.IPv4Mask == null)
                        continue;
                    Console.WriteLine("IP: " + uipi.Address + ", Netmask: " + uipi.IPv4Mask);
                    string[] IPParts = uipi.Address.ToString().Split('.');
                    string[] NetmaskParts = uipi.IPv4Mask.ToString().Split('.');
                    string StartIP;
                    StartIP = (int.Parse(IPParts[0]) & (int.Parse(NetmaskParts[0]))) + "." + (int.Parse(IPParts[1]) & (int.Parse(NetmaskParts[1]))) + "." + (int.Parse(IPParts[2]) & (int.Parse(NetmaskParts[2]))) + "." + (int.Parse(IPParts[3]) & (int.Parse(NetmaskParts[3])));
                    string EndIP;
                    string[] StartIPParts = StartIP.Split('.');
                    EndIP = (int.Parse(StartIPParts[0]) + 255 - (int.Parse(NetmaskParts[0]))) + "." + (int.Parse(StartIPParts[1]) + 255 - (int.Parse(NetmaskParts[1]))) + "." + (int.Parse(StartIPParts[2]) + 255 - (int.Parse(NetmaskParts[2]))) + "." + (int.Parse(StartIPParts[3]) + 255 - (int.Parse(NetmaskParts[3])));
                    Console.WriteLine("StartIP: " + StartIP);
                    Console.WriteLine("EndIP : " + EndIP);
                    string ItemIP, ItemMAC, ItemName;
                    for (int o0 = int.Parse(StartIP.Split('.')[0]); o0 <= int.Parse(EndIP.Split('.')[0]); o0++)
                        for (int o1 = int.Parse(StartIP.Split('.')[1]); o1 <= int.Parse(EndIP.Split('.')[1]); o1++)
                            for (int o2 = int.Parse(StartIP.Split('.')[2]); o2 <= int.Parse(EndIP.Split('.')[2]); o2++)
                                for (int o3 = int.Parse(StartIP.Split('.')[3]); o3 <= int.Parse(EndIP.Split('.')[3]); o3++)
                                {
                                    if ((o3 == 0) || (o3 == 255))
                                        continue;
                                    string MAC = GetMacFromIP(IPAddress.Parse(o0 + "." + o1 + "." + o2 + "." + o3));
                                    if (MAC == "00:00:00:00:00:00")
                                        continue;
                                    ItemIP = o0 + "." + o1 + "." + o2 + "." + o3;
                                    ItemMAC = GetMacFromIP(IPAddress.Parse(o0 + "." + o1 + "." + o2 + "." + o3));
                                    string[] Item = new string[2];
                                    Item[0] = ItemMAC;
                                    //You can add Item[] to any collection
                                    Item[1] = ItemIP;
                                    //Console.WriteLine(Item[0] + " --> " + Item[1]);
                                    MessageBox.Show(Item[0] + " --> " + Item[1]);
                                }
                }
            }
            Console.WriteLine("Scan Ended");
        }

        public static string GetMacFromIP(IPAddress IP)
        {
            if (IP.AddressFamily != AddressFamily.InterNetwork)
            {
                throw new ArgumentException("supports just IPv4 addresses");
            }

            int addrInt = IpToInt(IP);
            int srcAddrInt = IpToInt(IP);

            byte[] mac = new byte[6]; // 48 bit
            int length = mac.Length;
            int reply = SendArp(addrInt, srcAddrInt, mac, ref length);

            string rawMac = new PhysicalAddress(mac).ToString();
            string newMac = Regex.Replace(rawMac, "(..)(..)(..)(..)(..)(..)", "$1:$2:$3:$4:$5:$6");

            return newMac;
        }

        private static int IpToInt(IPAddress IP)
        {
            byte[] bytes = IP.GetAddressBytes();
            return BitConverter.ToInt32(bytes, 0);
        }
    }
}
